<?php 
     include './php/class.conexion.php';
    $nombre  = $_GET['usuario'];
    $sql  = "Select * from usuarios WHERE usuario='$nombre' ";
    $query = $conexion->query($sql);
    $idUsuario = null;
    while($respuesta = $query->fetch_array()){
        $idUsuario = $respuesta[0];
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Preguntas</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.4.3/dist/css/foundation.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/motion-ui@1.2.3/dist/motion-ui.min.css" />
    <link rel="stylesheet" href="css/index.css">
</head>

<body>
    <div class="grid-x">
        <div class="large-8 medium-8 small-12  cell medium-offset-2 large-offset-2">
            <div class="expanded button-group">
                <a class="button" onclick="elegirIdioma(1)">Español / Spanish</a>
                <a class="button" onclick="elegirIdioma(2)">Ingles / English</a>
            </div>
            <div class="grid-x contenedor-imagen align-middle" style="height: 30rem;" id="logoBosh">
                <img src="img/Bosch-brand.svg">
            </div>
            <div class="contenedor-espaniol" id="contenedorEspañol">
                <div class="contenedor estilos-contnedor-imagen">
                    <img src="img/ngs.png" class="alturas-imagenes">
                    <img src="img/ips.png" class="alturas-imagenes">
                    <img src="img/isd.png" class="alturas-imagenes">
                    <img src="img/isp.png" class="alturas-imagenes">

                </div>
                <form class="fondo-formulario">
                    <input type="text" class="ocultar-elementos" name="idUsuario" id="idUsuario" value="<?php echo $idUsuario; ?>">
                    <div id="pregunta1Espanol"></div>
                    <div id="respuesta1Espanol"></div>
                    <div id="pregunta2Espanol"></div>
                    <div id="respuesta2Espanol"></div>
                    <div id="pregunta3Espanol"></div>
                    <div id="respuesta3Espanol"></div>
                </form>
            </div>
            <div class="contenedor-ingles" id="contenedorIngles">
                <div class="contenedor estilos-contnedor-imagen">
                    <img src="img/ngs.png" class="alturas-imagenes">
                    <img src="img/ips.png" class="alturas-imagenes">
                    <img src="img/isd.png" class="alturas-imagenes">
                    <img src="img/isp.png" class="alturas-imagenes">
                </div>
                <form class="fondo-formulario">
                    <input type="text" class="ocultar-elementos" name="idUsuario" id="idUsuario" value="<?php echo $idUsuario; ?>">
                    <div id="pregunta1"></div>
                    <div id="respuesta1"></div>
                    <div id="pregunta2"></div>
                    <div id="respuesta2"></div>
                    <div id="pregunta3"></div>
                    <div id="respuesta3"></div>
                </form>
            </div>
        </div>
        <div class="large-4 medium-4 cell small-12 medium-offset-4 large-offset-4 fondo-formulario">
            <button class="button expanded" id="botonIngles" onclick="comprobar()">Check Answers</button>
            <button class="button expanded" id="botonEspañol" onclick="comprobarEspaniol()">Comprobar Respuestas</button>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js "></script>
    <script src="js/index.js "></script>
</body>

</html>