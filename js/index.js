$(document).ready(function() {
    $("#contenedorEspañol").css("display", "none");
    $("#botonEspañol").css("display", "none");
    $("#contenedorIngles").css("display", "none");
    $("#botonIngles").css("display", "none");
});

$("#FormularioCrearUsuario").submit((e) => {
    let objetoUsuario = {
        nombre: this.nombre.value,
        opcion: "crearUsuario"
    };
    if (!isEmpty(objetoUsuario.nombre)) {
        $.ajax({
            data: objetoUsuario,
            url: 'php/casos.php',
            type: 'post',
            success: (response) => {
                if (response.estado == 'ok')
                    location.href = 'quiz.php?usuario=' + response.mensaje
            }
        });
    } else
        swal({
            text: "Escribe algo ",
            buttons: false,
            timer: 2000
        });
    return false;
});

var isEmpty = (input) => (input.length === 0) ? true : false;

var elegirIdioma = (opcionElegida) => {
    switch (opcionElegida) {
        case 1:
            idiomaEspanol();
            $("#logoBosh").addClass("ocultar-elementos");
            $("#contenedorEspañol").css("display", "initial");
            $("#botonEspañol").css("display", "initial");
            $("#contenedorIngles").css("display", "none");
            $("#botonIngles").css("display", "none");
            break;

        case 2:
            idiomaIngles();
            $("#logoBosh").addClass("ocultar-elementos");
            $("#contenedorEspañol").css("display", "none");
            $("#botonEspañol").css("display", "none");
            $("#contenedorIngles").css("display", "initial");
            $("#botonIngles").css("display", "initial");
            break;
    }
}

function idiomaEspanol() {
    arrageloRes = [];
    let preguntas = [
        "¿Qué significa NGS?",
        "¿Cuál fue la primera plataforma de servicio lanzada desde NGS?",
        "¿Qué se esfuerza NGS por brindar?",
        "¿Cuántos canales de servicio personalizados ofrece NGS?",
        "¿De qué canal puede solicitar un servicio de TI?",
        "¿Dónde puedes encontrar artículos de autoayuda?",
        "¿Dónde se puede iniciar un chat instantáneo con un agente de soporte de?",
        "¿Cuál es la visión de los \" Servicios de soporte de próxima generación \"?",
        "¿Dónde puedo encontrar el flujo de trabajo de aprobación para una solicitud de TI abierta?",
        "¿Dónde puedo ver el tiempo real el cumplimiento del servicio de la solicitud abierta de TI?",
        "¿Cuál es el lema del Portal de servicios de TI?",
        "¿Cuál es el eslogan de IT Service Desk?",
        "¿Cuál es el eslogan para el servicio personal de TI?",
        "¿Cuáles son los lemas para cada canal de servicios de soporte de próxima generación?",
        "¿Cómo puedo acceder a IT Service Portal?",
        "¿Cómo puede proporcionar nuevas ideas para mejorar los servicios de TI de NGS?",
        "NGS apunta a la estandarización del servicio y .......?",
        "¿Por qué es necesario NGS?",
        "¿Cuáles son los beneficios generales de NGS?",
        "¿Cómo puede solicitar servicios en nombre de otros en el Portal de servicios de TI?"
    ];
    let respuestas = [
        [" Servicios de soporte de próxima generación ", " Servicios de próxima generación ", " Servicios de TI de próxima generación ", " Soporte de próxima generación "],
        [" Portal de servicios de TI ", " Servicio personal de TI ", " Mesa de servicio de TI ", " Servicio avanzado de TI "],
        [" Canales de entrada bien definidos para facilitar la búsqueda, solicitud, recepción y gestión de todo tipo de productos, servicios y asistencia técnica de TI en Bosch ", " PC estándar en el lugar de trabajo ", " Nuevos dispositivos ", " Un juego de herramientas para el lugar de trabajo móvil "],
        [" 3", "4", "2", "5"],
        [" Portal de servicios de TI ", " Mesa de servicio de TI ", " Servicio personal de TI ", " Servicio personal de TI ", " Espacio IT "],
        [" Página de soporte de TI en el portal de servicio de TI ", " Escritorio de servicio de TI ", " Servicio personal de TI ", " Espacio de TI "],
        [" Página de soporte de TI en el portal de servicio de TI ", " Escritorio de servicio de TI ", " Servicio personal de TI ", " Espacio de TI "],
        [" Experiencia de usuario ", " Generación de valor agregado para la empresa ", " Proporcionar asistencia instantánea ", " Proporcionar canales de comunicación modernos "],
        [" \" Mis pedidos\" en el portal de servicios de TI ", " \" Mis pedidos\" en Bosch Connect ", " \" Mis pedidos \" en el espacio de TI ", " \" Mis pedidos \" en el IT BGN "],
        [" \" Mis pedidos\" en el servicio de TI Portal ", " \" Mis pedidos\" en Bosch Connect ", " \" Mis pedidos \" en el espacio de TI ", " \" Mis pedidos \" en el IT BGN "],
        [" Es fácil ", " Es personal ", " Es interactivo ", " Es fácil de usar "],
        [" Es interactivo ", " Es personal ", " Es fácil ", " Es fácil de usar "],
        [" Es personal ", " Es fácil ", " Es interactivo ", " Es fácil de usar "],
        [" Todas ", " Es personal ", " Es interactivo ", " Es fácil de usar "],
        [" Simplemente escriba itsp.bosch.com en mi navegador ", " El botón  \"Portal de servicios de TI \" en kit de \" herramientas de trabajo IT \" ", " El botón \" Portal de servicios de TI \" en \" Mi trabajo \"en BGN", " De Bosch Connect "],
        [" Todas ", " Enviar por correo electrónico al agente de la mesa de servicio de TI ", " Informar al consultor en el área de TI ", " Blog de ideación en la comunidad de NGW/NGS "],
        [" Optimización ", " Colaboración ", " Cooperación ", " Satisfacción "],
        [" Todas ", " Falta claridad de canales de entrada ", " El concepto de soporte de IT mejorado ", " Responsabilidad poco clara que causa confusión para los clientes "],
        [" Todas ", " Entrega de servicios más rápida ", " Experiencia de usuario mejorada ", " Canales de soporte claramente definidos y más eficientes"],
        [" Buscar el botón \" pedir para otros \" en la página de productos / servicios detallados / detallados ", " No es posible pedir otros ", " Desde la \" página de mi aprobación\" ", " Desde la \" Pagina de soporte\" "]
    ];
    obtenerIndiceRespuestaCorrecta("respuestas1Espanol", "#pregunta1Espanol", "#respuesta1Espanol", respuestas, preguntas);
    obtenerIndiceRespuestaCorrecta("respuestas2Espanol", "#pregunta2Espanol", "#respuesta2Espanol", respuestas, preguntas);
    obtenerIndiceRespuestaCorrecta("respuestas3Espanol", "#pregunta3Espanol", "#respuesta3Espanol", respuestas, preguntas);
}

function idiomaIngles() {
    arrageloRes = [];
    let preguntas = [
        "What does NGS stand for? ",
        "What was the 1st service platform released from NGS? ",
        "What does NGS strive to provide? ",
        "How many customized service channels are offered by NGS? ",
        "Which channel can you order an IT service from? ",
        "Where can you find self-help articles? ",
        "Where can you start an instant message with a support agent from? ",
        "What's the vision of \" Next Generation Support Services \" ?",
        "Where can I find the approval workflow for an open IT request? ",
        "Where can I see the service actual fulfillment time for open IT request? ",
        "What is the slogan for IT Service Portal? ",
        "What is the slogan for IT Service Desk? ",
        "What is the slogan for IT Personal Service? ",
        "What are the slogans for each channel of Next Generation Support Services? ",
        "How can I access IT Service Portal? ",
        "How can you provide new ideas to improve NGS IT services? ",
        "NGS aims for service standardization and .......? ",
        "Why is NGS necessary? ",
        "What are the general benefits of NGS? ",
        "How can you order services on behalf of others in the IT Service Portal? "
    ];

    let respuestas = [
        [" Next Generation Support Services ", " Next Generation Services ", " Next Generation IT Services ", " Next Generation Support "],
        [" IT Service Portal ", " IT Personal Service ", " IT Service Desk ", " IT Advanced Service "],
        [" Well-defined entry channels to make it easy for you to find, request, receive and manage all kinds of IT products, services and support at Bosch. ", "Standard workplace PCs ", " New devices ", " A mobile workplace toolkit "],
        [" 3 ", " 4 ", " 2 ", " 5 "],
        [" IT Service Portal ", " IT Service Desk ", " IT Personal Service ", " IT Space "],
        [" IT Support Page in the IT Service Portal ", "IT Service Desk ", "IT Personal Service ", " IT Space "],
        [" IT Support Page in the IT Service Portal ", " IT Service Desk ", " IT Personal Service ", " IT Space "],
        [" User experience ", " Generating added value to the business  ", "  Provide instant support ", " Provide modern communication channels "],
        [" \"My Orders\" in the IT Service Portal", " \"My Orders\" in the Bosch Connect", " \"My Orders\" in the IT Space", " \"My Orders\" in the IT BGN"],
        [" \"My Orders\" in the IT Service Porta ", " \"My Orders\" in the Bosch Connect", " \"My Orders\" in the IT Space", "  \"My Orders\" in the IT BGN"],
        [" It’s easy ", " It's personal ", " It's interactive ", " It's user friendly "],
        [" It's interactive ", " It's personal ", " It’s easy ", " It's user friendly "],
        [" It's personal ", " It’s easy ", " It's interactive ", " It's user friendly "],
        [" All", "It's personal", "It's interactive", "It's user friendly"],
        [" Just type itsp.bosch.com into my browser ", " The \"IT Service Portal\" button in the \"IT Workplace Toolkit\" ", " The \"IT Service Portal\" button under the \"My work\" in BGN", "All"],
        [" Ideation blog in NGW/NGS community ", " Email to IT Service Desk Agent ", " Tell Consultant in IT Space ", " No channel for submitting new idea "],
        [" Optimization ", " Collaboration ", " Cooperation ", " Satisfaction "],
        [" All ", " Missing clarity of entry channels ", " The concept of improved IT support ", " Unclear responsibility causing confusion for customers "],
        [" All ", " Faster service delivery ", " Improved User Experience ", " Clearly defined and more efficient support channels "],
        [" Find the \"order for others\" button on pointed/detailed product/service item page", " It's not possible to order for others ", " From the \"my approval\" page ", " From the \"Support page\" "]

    ];
    obtenerIndiceRespuestaCorrecta("respuestas1", "#pregunta1", "#respuesta1", respuestas, preguntas);
    obtenerIndiceRespuestaCorrecta("respuestas2", "#pregunta2", "#respuesta2", respuestas, preguntas);
    obtenerIndiceRespuestaCorrecta("respuestas3", "#pregunta3", "#respuesta3", respuestas, preguntas);
}

var arrageloRes = [];

function obtenerIndiceRespuestaCorrecta(idinputRespuesta, idPreguntaADibujar, idRespuestaADibujar, arregloRespuestas, arregloPreguntas) {
    let indiceRespuestaCorrecta;
    let indiceAleatorio = Math.floor(Math.random() * arregloPreguntas.length);
    let respuestasPosibles = arregloRespuestas[indiceAleatorio];
    let posiciones = [0, 1, 2, 3];
    let respuestasReordenadas = [];
    let yaExiste = false;
    for (i in respuestasPosibles) {
        let posicionAleatoria = Math.floor(Math.random() * posiciones.length);
        if (posicionAleatoria == 0 && yaExiste == false) {
            yaExiste = true;
            indiceRespuestaCorrecta = i;
            arrageloRes.push(indiceRespuestaCorrecta);
        }
        respuestasReordenadas[i] = respuestasPosibles[posiciones[posicionAleatoria]];
        posiciones.splice(posicionAleatoria, 1);
    }
    let txtRespuestas = "";
    for (i in respuestasReordenadas) {
        txtRespuestas += `<input type="radio" id="${ idinputRespuesta }" name="${ idinputRespuesta }" value='${ i }'><label>${ respuestasReordenadas[i] }</label><br>`;
    }
    rellenarPreguntas(idPreguntaADibujar, arregloPreguntas[indiceAleatorio]);
    rellenarRespuestas(idRespuestaADibujar, txtRespuestas);
}

var rellenarPreguntas = (idPregunta, preguntas) => $(idPregunta).html(`<strong><p>${ preguntas }</p></strong>`);
var rellenarRespuestas = (idRespuesta, respuestas) => $(idRespuesta).html(respuestas);

var comprobar = () => {
    let respuesta1 = obtenerValorSeleccionado("#respuestas1");
    let respuesta2 = obtenerValorSeleccionado("#respuestas2");
    let respuesta3 = obtenerValorSeleccionado("#respuestas3");
    let idUsuario = $("#idUsuario").val();
    if (respuesta1 != undefined && respuesta2 != undefined && respuesta3 != undefined) {
        if (respuesta1 == arrageloRes[0] && respuesta2 == arrageloRes[1] && respuesta3 == arrageloRes[2]) {
            regalos("ingles", idUsuario);
        } else {
            swal({
                text: "try again",
                icon: "info",
                button: true
            }).then((ok) => {
                if (ok)
                    actualizarArticulo("ingles", idUsuario);
            })
            $("#logoBosh").css('display', 'flex');
            $("#contenedorEspañol").css('display', 'none');
            $("#contenedorIngles").css('display', 'none');
            $("#botonIngles").css('display', 'none');
            $("#botonEspañol").css('display', 'none');
            $("#logoBosh").css('height', '30rem')
        }
    } else
        alertasDulces("empty imputs ", "check answers!", "warning");
}

var comprobarEspaniol = () => {
    let respuesta1 = obtenerValorSeleccionado("#respuestas1Espanol");
    let respuesta2 = obtenerValorSeleccionado("#respuestas2Espanol");
    let respuesta3 = obtenerValorSeleccionado("#respuestas3Espanol");
    let idUsuario = $("#idUsuario").val();
    if (respuesta1 != undefined && respuesta2 != undefined && respuesta3 != undefined) {
        if (respuesta1 == arrageloRes[0] && respuesta2 == arrageloRes[1] && respuesta3 == arrageloRes[2]) {
            regalos("español", idUsuario);
        } else {
            swal({
                text: "Intenta de nuevo",
                icon: "info",
                button: "Aceptar"
            }).then((aceptar) => {
                if (aceptar)
                    actualizarArticulo("español", idUsuario);
            })
            $("#logoBosh").css('display', 'flex');
            $("#contenedorEspañol").css('display', 'none');
            $("#contenedorIngles").css('display', 'none');
            $("#botonIngles").css('display', 'none');
            $("#botonEspañol").css('display', 'none');
            $("#logoBosh").css('height', '30rem');
        }
    } else
        alertasDulces("Algun campo esta vacio :| ", "Por favor revisa tus respuestas!", "warning");
}

var actualizarArticulo = (lenguaje, idUsuario) => {
    $.ajax({
        data: {
            opcion: 'registrarUsuarioArticulo',
            idUsuario: idUsuario
        },
        url: 'php/casos.php',
        type: 'post',
        success: (response) => {
            if (response.estado)
                location.href = 'index.html'
        }
    });
}

var regalos = (lenguaje, idUsuario) => {
    $.ajax({
        data: {
            opcion: 'cargarRegalo'
        },
        url: 'php/casos.php',
        type: 'post',
        success: (respuesta) => {
            if (respuesta.estado == 'ok') {
                let array = [];
                respuesta.mensaje.forEach((articulo) => {
                    if (articulo.totalArticulo != 0)
                        array.push(articulo)
                });
                if (array.length != 0) {
                    let articuloSelect = Math.floor(Math.random() * array.length);

                    if (lenguaje == "español") {
                        swal({
                            title: "Tu regalo es un : " + array[articuloSelect].nombreArticulo,
                            text: "sobran : " + array[articuloSelect].totalArticulo,
                            icon: "success",
                            button: "Aceptar"
                        }).then((aceptar) => {
                            if (aceptar)
                                location.href = "index.html"
                        });
                        $("#logoBosh").css('display', 'flex');
                        $("#contenedorEspañol").css('display', 'none');
                        $("#contenedorIngles").css('display', 'none');
                        $("#botonIngles").css('display', 'none');
                        $("#botonEspañol").css('display', 'none');
                        $("#logoBosh").css('height', '30rem');
                    } else {
                        swal({
                            title: "your gift is :  " + traducirRegalos(array[articuloSelect].nombreArticulo),
                            text: "There are too many : " + array[articuloSelect].totalArticulo,
                            icon: "success",
                            button: "Aceptar"
                        }).then((aceptar) => {
                            if (aceptar)
                                location.href = "index.html"
                        });
                        $("#logoBosh").css('display', 'flex');
                        $("#contenedorEspañol").css('display', 'none');
                        $("#contenedorIngles").css('display', 'none');
                        $("#botonIngles").css('display', 'none');
                        $("#botonEspañol").css('display', 'none');
                        $("#logoBosh").css('height', '30rem');
                    }

                    $.ajax({
                        data: {
                            opcion: 'descontarRegalo',
                            idProducto: array[articuloSelect].idRegalo,
                            idUsuario: idUsuario
                        },
                        url: 'php/casos.php',
                        type: 'post'
                    });
                } else {
                    if (lenguaje == 'español') {
                        swal({
                            text: "se han agotado todos los regalos",
                            button: true
                        }).then((aceptar) => {
                            if (aceptar)
                                location.href = "index.html";
                        });
                    } else {
                        swal({
                            text: "all gifts have been exhausted",
                            button: true
                        }).then((aceptar) => {
                            if (aceptar)
                                location.href = "index.html";
                        });
                    }
                }
            }
        }
    });
}

var traducirRegalos = (regaloEnIdiomaEspañol) => {
    let regresarRegaloEnIngles;
    switch (regaloEnIdiomaEspañol) {
        case 'Playera':
            regresarRegaloEnIngles = "T-shirt";
            break;
        case 'Adaptador':
            regresarRegaloEnIngles = "Adapter";
            break;
        case 'Boligrafo':
            regresarRegaloEnIngles = "Pen";
            break;
        case 'Taza':
            regresarRegaloEnIngles = "Cup";
            break;
        case 'Pelota anti-estres':
            regresarRegaloEnIngles = "anti-stress ball";
            break;
    }
    return regresarRegaloEnIngles;
}
var obtenerValorSeleccionado = (idInput) => $('input' + idInput + '[type=radio]:checked').val();

var alertasDulces = (mensaje1, mensaje2, tipoDeMensaje) => swal(mensaje1, mensaje2, tipoDeMensaje);