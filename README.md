## Descripción del repositorio ##

* Este repositorio contiene un ejemplo de un quiz que contine preguntas aleatoriamente.

## Version del proyecto ##

* 2.1

## Lenguajes de programacion usados ##

* JavaScript, PHP, HTML, CSS.

## Frameworks utilizados ##

* Framework que se utilizo en css fue [Fundation] (https://foundation.zurb.com/)

## Librerias utilizadas ##

* Libreria de [sweetAlert] (https://sweetalert.js.org/guides/)